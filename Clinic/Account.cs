﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Clinic
{
    public partial class Account : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=hospital_records;Integrated Security=True");
        int accountID;

        public Account()
        {
            InitializeComponent();
        }

        //Showing datagridview data
        public void ShowAccounts()
        {
            connection.Open();
            SqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT * FROM user_accounts";
            DataTable table = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(comm);
            adapter.Fill(table);
            dgvAccounts.DataSource = table;
            connection.Close();
        }
        //Loading the page
        private void Account_Load(object sender, EventArgs e)
        {
            ShowAccounts();
            txtUsername.Text = "";
            txtPassword.Text = "";
            cmbConfirm.Text = "";
            cmbStatus.Text = "";
            cmbType.Text = "";
        }
        //Selecting the row and getting the id
        private void DgvAccounts_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            accountID = Convert.ToInt16(dgvAccounts.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtUsername.Text = dgvAccounts.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtPassword.Text = dgvAccounts.Rows[e.RowIndex].Cells[2].Value.ToString();
            cmbConfirm.Text = dgvAccounts.Rows[e.RowIndex].Cells[3].Value.ToString();
            cmbStatus.Text = dgvAccounts.Rows[e.RowIndex].Cells[4].Value.ToString();
            cmbType.Text = dgvAccounts.Rows[e.RowIndex].Cells[5].Value.ToString();
        }


        //Adding an account
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text != "" && txtPassword.Text != "" && cmbConfirm.Text != "" && cmbStatus.Text != "" && cmbType.Text != "")
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to add this account?", "Insert Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"INSERT INTO user_accounts(account_username, account_password, account_confirmation, account_status, user_type) VALUES('{txtUsername.Text}', '{txtPassword.Text}', '{cmbConfirm.Text}', '{cmbStatus.Text}', '{cmbType.Text}');");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("Insert Complete!");
                        txtUsername.Text = "";
                        txtPassword.Text = "";
                        cmbConfirm.Text = "";
                        cmbStatus.Text = "";
                        cmbType.Text = "";
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else
                        {
                            txtUsername.Text = "";
                            txtPassword.Text = "";
                            cmbConfirm.Text = "";
                            cmbStatus.Text = "";
                            cmbType.Text = "";
                        }
                        connection.Close();
                    }
                    ShowAccounts();
                }
                else
                {
                    string title = "Empty";
                    string caption = "Please fill out the form";
                    MessageBox.Show(caption, title);
                }
            }
        }


        //Updating account
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult answer = MessageBox.Show("Are you sure you want to update this account?", "Update Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (txtUsername.Text != "" && txtPassword.Text != "" && cmbConfirm.Text != "" && cmbStatus.Text != "" && cmbType.Text != "")
            {
                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"UPDATE user_accounts SET account_username = '{txtUsername.Text}', account_password = '{txtPassword.Text}', account_confirmation = '{cmbConfirm.Text}', account_status = '{cmbStatus.Text}', user_type = '{cmbType.Text}' WHERE account_id = {accountID};");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        ShowAccounts();
                        MessageBox.Show("Update was successful :)");
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else { }
                        connection.Close();
                    }
                }
            }
            txtUsername.Text = "";
            txtPassword.Text = "";
            cmbConfirm.Text = "";
            cmbStatus.Text = "";
            cmbType.Text = "";
        }

        //Deleting an account
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult answer = MessageBox.Show("Are you sure you want to delete this account?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (txtUsername.Text != "" && txtPassword.Text != "" && cmbConfirm.Text != "" && cmbStatus.Text != "" && cmbType.Text != "")
            {
                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"DELETE FROM user_accounts WHERE account_id = {accountID};");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        ShowAccounts();
                        MessageBox.Show("Account deleted.");
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else { }
                        connection.Close();
                    }
                }
            }
            txtUsername.Text = "";
            txtPassword.Text = "";
            cmbConfirm.Text = "";
            cmbStatus.Text = "";
            cmbType.Text = "";
        }


        //Exiting the page
        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminManager adminPage = new AdminManager();
            adminPage.Show();
        }


    }
}
