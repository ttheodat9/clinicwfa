﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Clinic
{
    public partial class Search : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=hospital_records;Integrated Security=True");
        public bool doctorSearch = false;
        public Search()
        {
            InitializeComponent();
        }

        //Showing options for the combo box based on boolean results
        private DataTable GetAll()
        {
            DataTable table = null;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                if (doctorSearch.ToString() == "True")
                {
                    comm.CommandText = "SELECT DISTINCT (doctor_first_name + ' ' + doctor_last_name) AS Doctor FROM doctors";
                }
                else
                {
                    comm.CommandText = "SELECT DISTINCT (patient_first_name + ' ' + patient_last_name) AS Patient FROM patients";
                }
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                table = new DataTable();
                adapter.Fill(table);
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return table;
        }
        private void Search_Load(object sender, EventArgs e)
        {
            if (doctorSearch.ToString() == "True")
            {
                cmbDocPatName.DataSource = GetAll();
                cmbDocPatName.DisplayMember = "Doctor";
                DisplayResults();
            }
            else
            {
                cmbDocPatName.DataSource = GetAll();
                cmbDocPatName.DisplayMember = "Patient";
                DisplayResults();
            }
        }

        //Filling the datagridview
        public void DisplayResults()
        {
            try
            {
                string queryStr;
                if (doctorSearch.ToString() == "True")
                {
                    queryStr = $"SELECT * FROM doctors WHERE (doctor_first_name + ' ' + doctor_last_name) LIKE '{cmbDocPatName.Text}'";
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = queryStr;
                    SqlDataAdapter adapter = new SqlDataAdapter(comm);
                    DataTable table = new DataTable();
                    adapter.Fill(table);
                    dgvDocPat.DataSource = table;
                    connection.Close();
                }
                else
                {
                    queryStr = $"SELECT * FROM patients WHERE (patient_first_name + ' ' + patient_last_name) LIKE '{cmbDocPatName.Text}'";
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = queryStr;
                    SqlDataAdapter adapter = new SqlDataAdapter(comm);
                    DataTable table = new DataTable();
                    adapter.Fill(table);
                    dgvDocPat.DataSource = table;
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                connection.Close();
            }
        }

        //Returning to previous page
        private void BtnBack_Click(object sender, EventArgs e)
        {
            if (doctorSearch.ToString() == "True")
            {
                this.Hide();
                Doctor doctorPage = new Doctor();
                doctorPage.Show();
            }
            else
            {
                this.Hide();
                Patients patient = new Patients();
                patient.Show();
            }
        }

        //Showing results when combo choice is changed
        private void CmbDocPatName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayResults();
        }
    }

}
