﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Clinic
{
    public partial class Doctor : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=hospital_records;Integrated Security=True");
        int doctorID;
        public Doctor()
        {
            InitializeComponent();
        }

        //Showing datagridview data
        public void ShowDoctors()
        {
            connection.Open();
            SqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT * FROM doctors";
            DataTable table = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(comm);
            adapter.Fill(table);
            dgvDoctors.DataSource = table;
            connection.Close();
        }
        //Loading the datagridview
        private void Doctor_Load(object sender, EventArgs e)
        {
            ShowDoctors();
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtPhone.Text = "";
            cmbSpecialty.Text = "";
        }
        //Selecting the row and getting the id
        private void DgvDoctors_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            doctorID = Convert.ToInt16(dgvDoctors.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtFirstName.Text = dgvDoctors.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtLastName.Text = dgvDoctors.Rows[e.RowIndex].Cells[2].Value.ToString();
            cmbSpecialty.Text = dgvDoctors.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtPhone.Text = dgvDoctors.Rows[e.RowIndex].Cells[4].Value.ToString();
        }


        //Inserting data
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (txtFirstName.Text != "" && txtLastName.Text != "" && txtPhone.Text != "" && cmbSpecialty.Text != "")
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to insert this doctor's info?", "Insert Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"INSERT INTO doctors(doctor_first_name, doctor_last_name, doctor_specialty, doctor_phone) VALUES('{txtFirstName.Text}', '{txtLastName.Text}', '{cmbSpecialty.Text}', '{txtPhone.Text}');");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("Insert Complete!");
                        ShowDoctors();
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else
                        {
                            txtFirstName.Text = "";
                            txtLastName.Text = "";
                            txtPhone.Text = "";
                            cmbSpecialty.Text = "";
                        }
                        connection.Close();
                    }
                }
                else
                {
                    string title = "Empty";
                    string caption = "Please fill out the form";
                    MessageBox.Show(caption, title);
                }
            }
        }


        //Updating the data
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult answer = MessageBox.Show("Are you sure you want to update this doctor's info?", "Update Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (txtFirstName.Text != "" && txtLastName.Text != "" && txtPhone.Text != "" && cmbSpecialty.Text != "")
            {
                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"UPDATE doctors SET doctor_first_name = '{txtFirstName.Text}', doctor_last_name = '{txtLastName.Text}', doctor_specialty = '{cmbSpecialty.Text}', doctor_phone = '{txtPhone.Text}' WHERE doctor_id = {doctorID};");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        ShowDoctors();
                        MessageBox.Show("Update was successful :)");
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else
                        {
                            txtFirstName.Text = "";
                            txtLastName.Text = "";
                            txtPhone.Text = "";
                            cmbSpecialty.Text = "";
                        }
                        connection.Close();
                    }

                }
            }
        }


        //Deleting data
        private void BtnDelete_Click(object sender, EventArgs e)
        {

            if (txtFirstName.Text != "" && txtLastName.Text != "" && txtPhone.Text != "" && cmbSpecialty.Text != "")
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to delete this doctor's info?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"DELETE FROM doctors WHERE doctor_id = {doctorID};");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        ShowDoctors();
                        MessageBox.Show("Data was successfully deleted.");
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else
                        {
                            txtFirstName.Text = "";
                            txtLastName.Text = "";
                            txtPhone.Text = "";
                            cmbSpecialty.Text = "";
                        }
                        connection.Close();
                    }
                }
            }
        }

        //Exit
        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminManager adminPage = new AdminManager();
            adminPage.Show();
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            this.Hide();
            Search search = new Search();
            search.doctorSearch = true;
            search.Show();
        }
    }
}
