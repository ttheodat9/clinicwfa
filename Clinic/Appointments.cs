﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Clinic
{
    public partial class Appointments : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=hospital_records;Integrated Security=True");
        int appointmentID;
        public Appointments()
        {
            InitializeComponent();
        }

        //Showing datagridview data
        public void ShowAppointments()
        {
            connection.Open();
            SqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT * FROM staff_appointments";
            DataTable table = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(comm);
            adapter.Fill(table);
            dgvAppointments.DataSource = table;
            connection.Close();
        }
        //Getting the doctors' and patients' names
        private DataTable GetDoctorName()
        {
            DataTable table = null;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "SELECT DISTINCT (doctor_first_name + ' ' + doctor_last_name) AS FullName FROM doctors";
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                table = new DataTable();
                adapter.Fill(table);
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return table;
        }
        private DataTable GetPatientName()
        {
            DataTable table = null;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "SELECT DISTINCT (patient_first_name + ' ' + patient_last_name) AS fullName FROM patients";
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                table = new DataTable();
                adapter.Fill(table);
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return table;
        }
        //Loading the page
        private void Appointments_Load(object sender, EventArgs e)
        {
            ShowAppointments();
            txtAddress.Text = "";
            cmbStatus.Text = "";
            cmbDoctorName.DataSource = GetDoctorName();
            cmbDoctorName.DisplayMember = "FullName";
            cmbPatientName.DataSource = GetPatientName();
            cmbPatientName.DisplayMember = "fullName";
            cmbDoctorName.Text = "";
            cmbPatientName.Text = "";
        }
        //Selecting the row and getting the row id
        private void DgvAppointments_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            appointmentID = Convert.ToInt32(dgvAppointments.Rows[e.RowIndex].Cells[0].Value.ToString());
            cmbDoctorName.Text = dgvAppointments.Rows[e.RowIndex].Cells[1].Value.ToString();
            cmbPatientName.Text = dgvAppointments.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtAddress.Text = dgvAppointments.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtDate.Text = (DateTime.Parse(dgvAppointments.Rows[e.RowIndex].Cells[4].Value.ToString())).ToString();
            dtTime.Text = (DateTime.Parse(dgvAppointments.Rows[e.RowIndex].Cells[5].Value.ToString())).ToString();
            cmbStatus.Text = dgvAppointments.Rows[e.RowIndex].Cells[6].Value.ToString();
        }



        //Adding Appoinments 
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (cmbDoctorName.Text != "" && cmbPatientName.Text != "" && txtAddress.Text != "" && cmbStatus.Text != "")
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to add this appointment?", "Insert Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    DateTime date = dtDate.Value;
                    DateTime time = dtTime.Value;
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"INSERT INTO staff_appointments(doctor_name, patient_name, appointment_location, appointment_date, appointment_time, appointment_status) VALUES('{cmbDoctorName.Text}', '{cmbPatientName.Text}', '{txtAddress.Text}', '{date}', '{time}','{cmbStatus.Text}');");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("The appointment has been added!");
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else { }
                        connection.Close();
                    }
                    ShowAppointments();
                }
                else
                {
                    string title = "Empty";
                    string caption = "Please fill out the form";
                    MessageBox.Show(caption, title);
                }
            }
            cmbDoctorName.Text = "";
            cmbPatientName.Text = "";
            txtAddress.Text = "";
            cmbStatus.Text = "";
        }

        //Updating Appointments
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            if (cmbDoctorName.Text != "" && cmbPatientName.Text != "" && txtAddress.Text != "" && cmbStatus.Text != "")
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to update this appointment?", "Update Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    DateTime date = dtDate.Value;
                    DateTime time = dtTime.Value;
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"UPDATE staff_appointments SET doctor_name = '{cmbDoctorName.Text}', patient_name = '{cmbPatientName.Text}', appointment_location = '{txtAddress.Text}', appointment_date = '{date}', appointment_time = '{time}', appointment_status = '{cmbStatus.Text}' WHERE appointment_id = {appointmentID};");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("The appointment has been updated!");
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else { }
                        connection.Close();
                    }
                    ShowAppointments();
                }
                else
                {
                    string title = "Empty";
                    string caption = "Please fill out the form";
                    MessageBox.Show(caption, title);
                }
            }
            cmbDoctorName.Text = "";
            cmbPatientName.Text = "";
            txtAddress.Text = "";
            cmbStatus.Text = "";
        }

        //Deleting Appointments
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (cmbDoctorName.Text != "" && cmbPatientName.Text != "" && txtAddress.Text != "" && cmbStatus.Text != "")
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to delete this appointment?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"DELETE FROM staff_appointments WHERE appointment_id = {appointmentID};");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("The appointment has been deleted.");
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else { }
                        connection.Close();
                    }
                    ShowAppointments();
                }
                else
                {
                    string title = "Empty";
                    string caption = "Please fill out the form";
                    MessageBox.Show(caption, title);
                }
            }
            cmbDoctorName.Text = "";
            cmbPatientName.Text = "";
            txtAddress.Text = "";
            cmbStatus.Text = "";
        }


        //Exit
        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
            ClinicManager clinic = new ClinicManager();
            clinic.Show();
        }


    }
}
