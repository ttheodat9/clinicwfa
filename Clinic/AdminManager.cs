﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinic
{
    public partial class AdminManager : Form
    {
        public AdminManager()
        {
            InitializeComponent();
        }

        private void BtnDoctors_Click(object sender, EventArgs e)
        {
            Doctor doctorPage = new Doctor();
            this.Hide();
            doctorPage.Show();
        }

        private void BtnAccounts_Click(object sender, EventArgs e)
        {
            Account accountPage = new Account();
            this.Hide();
            accountPage.Show();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }

        private void BtnLogut_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 mainForm = new Form1();
            mainForm.Show();
        }
    }
}
