﻿namespace Clinic
{
    partial class Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBack = new System.Windows.Forms.Button();
            this.dgvDocPat = new System.Windows.Forms.DataGridView();
            this.cmbDocPatName = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocPat)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.LightGray;
            this.btnBack.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ForeColor = System.Drawing.Color.Crimson;
            this.btnBack.Location = new System.Drawing.Point(332, 346);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(131, 83);
            this.btnBack.TabIndex = 56;
            this.btnBack.Text = "Return to Previous Page";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // dgvDocPat
            // 
            this.dgvDocPat.AllowUserToAddRows = false;
            this.dgvDocPat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDocPat.BackgroundColor = System.Drawing.Color.PeachPuff;
            this.dgvDocPat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocPat.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgvDocPat.Location = new System.Drawing.Point(34, 150);
            this.dgvDocPat.Name = "dgvDocPat";
            this.dgvDocPat.ReadOnly = true;
            this.dgvDocPat.RowHeadersVisible = false;
            this.dgvDocPat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDocPat.Size = new System.Drawing.Size(732, 150);
            this.dgvDocPat.TabIndex = 57;
            // 
            // cmbDocPatName
            // 
            this.cmbDocPatName.BackColor = System.Drawing.Color.Coral;
            this.cmbDocPatName.FormattingEnabled = true;
            this.cmbDocPatName.Items.AddRange(new object[] {
            "Open",
            "Closed",
            "Delayed",
            "Missed",
            "Cancelled"});
            this.cmbDocPatName.Location = new System.Drawing.Point(266, 75);
            this.cmbDocPatName.Name = "cmbDocPatName";
            this.cmbDocPatName.Size = new System.Drawing.Size(268, 21);
            this.cmbDocPatName.TabIndex = 58;
            this.cmbDocPatName.SelectedIndexChanged += new System.EventHandler(this.CmbDocPatName_SelectedIndexChanged);
            // 
            // Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cmbDocPatName);
            this.Controls.Add(this.dgvDocPat);
            this.Controls.Add(this.btnBack);
            this.Name = "Search";
            this.Text = "Search";
            this.Load += new System.EventHandler(this.Search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocPat)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.DataGridView dgvDocPat;
        private System.Windows.Forms.ComboBox cmbDocPatName;
    }
}