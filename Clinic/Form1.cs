﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Clinic
{
    public partial class Form1 : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=hospital_records;Integrated Security=True");
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            ClinicManager clinic = new ClinicManager();
            AdminManager admin = new AdminManager();
            if (txtUsername.Text != "" && txtPassword.Text != "")
            {
                try
                {
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = String.Format($"SELECT * FROM user_accounts WHERE (account_username LIKE '{txtUsername.Text}' AND account_password LIKE '{txtPassword.Text}');");
                    comm.CommandType = CommandType.Text;
                    SqlDataReader reader = comm.ExecuteReader();


                    if (reader.Read() == true)
                    {
                        string User = (string)reader["account_username"];
                        string Pass = (string)reader["account_password"];
                        string userType = (string)reader["user_type"];

                        if (txtUsername.Text == User && txtPassword.Text == Pass)
                        {
                            this.Hide();
                            if (userType == "staff")
                            {
                                clinic.Show();
                            }
                            else if (userType == "administrator")
                            {
                                admin.Show();
                            }
                            txtUsername.Text = "";
                            txtPassword.Text = "";

                        }
                    }
                    else if (reader.Read() == false)
                    {
                        string title = "Login Error!";
                        string caption = "Incorrect username and/or password";
                        MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
                        MessageBoxIcon icons = MessageBoxIcon.Error;
                        DialogResult result = MessageBox.Show(caption, title, buttons, icons);
                        if (result == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                    }
                    connection.Close();
                }
                catch (SqlException)
                {
                    MessageBoxIcon icons = MessageBoxIcon.Warning;
                    MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                    DialogResult result = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icons);
                    if (result == DialogResult.Cancel)
                    {
                        this.Close();
                    }
                }
            }
            else
            {
                string title = "Empty";
                string caption = "Please fill out the form";
                MessageBox.Show(caption, title);
            }
        }
    }
}
