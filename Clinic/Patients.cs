﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Clinic
{
    public partial class Patients : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=hospital_records;Integrated Security=True");
        int patientID;
        public Patients()
        {
            InitializeComponent();
        }

        //Showing datagridview data
        public void ShowPatients()
        {
            connection.Open();
            SqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT * FROM patients";
            DataTable table = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(comm);
            adapter.Fill(table);
            dgvPatients.DataSource = table;
            connection.Close();
        }
        //Loading the datagridview
        private void Patients_Load(object sender, EventArgs e)
        {
            ShowPatients();
            txtFirstName.Text = "";
            txtLastName.Text = "";
            cmbGender.Text = "";
            txtAddress.Text = "";
            txtZipCode.Text = "_____";
            txtPhone.Text = "";
        }
        //Selecting the row and getting the id
        private void DgvPatients_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            patientID = Convert.ToInt16(dgvPatients.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtFirstName.Text = dgvPatients.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtLastName.Text = dgvPatients.Rows[e.RowIndex].Cells[2].Value.ToString();
            cmbGender.Text = dgvPatients.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtAddress.Text = dgvPatients.Rows[e.RowIndex].Cells[4].Value.ToString();
            txtZipCode.Text = dgvPatients.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtPhone.Text = dgvPatients.Rows[e.RowIndex].Cells[6].Value.ToString();
            dtDOB.Text = (DateTime.Parse(dgvPatients.Rows[e.RowIndex].Cells[7].Value.ToString())).ToString();
        }


        //Inserting data
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (txtFirstName.Text != "" && txtLastName.Text != "" && cmbGender.Text != "" && txtAddress.Text != "" && txtZipCode.Text != "_____" && txtPhone.Text != "")
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to insert this doctor's info?", "Insert Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    DateTime DOB = dtDOB.Value;
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"INSERT INTO patients(patient_first_name, patient_last_name, patient_gender, patient_address, patient_zip_code, patient_phone, patient_dob) VALUES('{txtFirstName.Text}', '{txtLastName.Text}', '{cmbGender.Text}', '{txtAddress.Text}', '{txtZipCode.Text}', '{txtPhone.Text}', '{DOB}');");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("Insert Complete!");
                        txtFirstName.Text = "";
                        txtLastName.Text = "";
                        cmbGender.Text = "";
                        txtAddress.Text = "";
                        txtZipCode.Text = "_____";
                        txtPhone.Text = "";
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else
                        {
                            txtFirstName.Text = "";
                            txtLastName.Text = "";
                            txtPhone.Text = "";
                            cmbGender.Text = "";
                        }
                        connection.Close();
                    }
                    ShowPatients();
                }
                else
                {
                    string title = "Empty";
                    string caption = "Please fill out the form";
                    MessageBox.Show(caption, title);
                }
            }
        }


        //Updating the data
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult answer = MessageBox.Show("Are you sure you want to update this patient's info?", "Update Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (txtFirstName.Text != "" && txtLastName.Text != "" && cmbGender.Text != "" && txtAddress.Text != "" && txtZipCode.Text != "_____" && txtPhone.Text != "")
            {
                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        DateTime DOB = dtDOB.Value;
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"UPDATE patients SET patient_first_name = '{txtFirstName.Text}', patient_last_name = '{txtLastName.Text}', patient_gender = '{cmbGender.Text}', patient_address = '{txtAddress.Text}', patient_zip_code = '{txtZipCode.Text}', patient_phone = '{txtPhone.Text}', patient_DOB = '{DOB}' WHERE patient_id = {patientID};");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        ShowPatients();
                        MessageBox.Show("Update was successful :)");
                        txtFirstName.Text = "";
                        txtLastName.Text = "";
                        cmbGender.Text = "";
                        txtAddress.Text = "";
                        txtZipCode.Text = "_____";
                        txtPhone.Text = "";
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else
                        {
                            txtFirstName.Text = "";
                            txtLastName.Text = "";
                            txtPhone.Text = "";
                            cmbGender.Text = "";
                        }
                        connection.Close();
                    }

                }
            }
        }


        //Deleting data
        private void BtnDelete_Click(object sender, EventArgs e)
        {

            if (txtFirstName.Text != "" && txtLastName.Text != "" && txtPhone.Text != "" && cmbGender.Text != "" && txtAddress.Text != "" && txtZipCode.Text != "_____" && txtPhone.Text != "")
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to delete this patient's info?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"DELETE FROM patients WHERE patient_id = {patientID};");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        ShowPatients();
                        MessageBox.Show("Data was successfully deleted.");
                        txtFirstName.Text = "";
                        txtLastName.Text = "";
                        cmbGender.Text = "";
                        txtAddress.Text = "";
                        txtZipCode.Text = "_____";
                        txtPhone.Text = "";
                    }
                    catch (SqlException)
                    {
                        MessageBoxButtons buttons = MessageBoxButtons.RetryCancel;
                        MessageBoxIcon icon = MessageBoxIcon.Warning;
                        DialogResult dialogResult = MessageBox.Show("An error occurred in the query.", "SQL Exception", buttons, icon);
                        if (dialogResult == DialogResult.Cancel)
                        {
                            this.Close();
                        }
                        else
                        {
                            txtFirstName.Text = "";
                            txtLastName.Text = "";
                            cmbGender.Text = "";
                            txtAddress.Text = "";
                            txtZipCode.Text = "_____";
                            txtPhone.Text = "";
                        }
                        connection.Close();
                    }
                }
            }
        }

        //Exit
        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
            ClinicManager clinicPage = new ClinicManager();
            clinicPage.Show();
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            this.Hide();
            Search search = new Search();
            search.Show();
        }
    }

}
