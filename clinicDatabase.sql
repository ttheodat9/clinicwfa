USE [master]
GO
/****** Object:  Database [hospital_records]    Script Date: 4/21/2021 7:35:58 AM ******/
CREATE DATABASE [hospital_records]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'hospital_records', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\hospital_records.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'hospital_records_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\hospital_records_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [hospital_records] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [hospital_records].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [hospital_records] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [hospital_records] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [hospital_records] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [hospital_records] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [hospital_records] SET ARITHABORT OFF 
GO
ALTER DATABASE [hospital_records] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [hospital_records] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [hospital_records] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [hospital_records] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [hospital_records] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [hospital_records] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [hospital_records] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [hospital_records] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [hospital_records] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [hospital_records] SET  DISABLE_BROKER 
GO
ALTER DATABASE [hospital_records] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [hospital_records] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [hospital_records] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [hospital_records] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [hospital_records] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [hospital_records] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [hospital_records] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [hospital_records] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [hospital_records] SET  MULTI_USER 
GO
ALTER DATABASE [hospital_records] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [hospital_records] SET DB_CHAINING OFF 
GO
ALTER DATABASE [hospital_records] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [hospital_records] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [hospital_records] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [hospital_records] SET QUERY_STORE = OFF
GO
USE [hospital_records]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [hospital_records]
GO
/****** Object:  Table [dbo].[doctors]    Script Date: 4/21/2021 7:36:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[doctors](
	[doctor_id] [int] IDENTITY(1,1) NOT NULL,
	[doctor_first_name] [varchar](50) NOT NULL,
	[doctor_last_name] [varchar](50) NOT NULL,
	[doctor_specialty] [varchar](50) NOT NULL,
	[doctor_phone] [varchar](50) NULL,
 CONSTRAINT [PK_doctors] PRIMARY KEY CLUSTERED 
(
	[doctor_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[patients]    Script Date: 4/21/2021 7:36:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[patients](
	[patient_id] [int] IDENTITY(1,3) NOT NULL,
	[patient_first_name] [varchar](50) NOT NULL,
	[patient_last_name] [varchar](50) NOT NULL,
	[patient_gender] [varchar](10) NULL,
	[patient_address] [varchar](150) NOT NULL,
	[patient_zip_code] [int] NOT NULL,
	[patient_phone] [varchar](50) NOT NULL,
	[patient_dob] [date] NOT NULL,
 CONSTRAINT [PK_patients] PRIMARY KEY CLUSTERED 
(
	[patient_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[staff_appointments]    Script Date: 4/21/2021 7:36:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[staff_appointments](
	[appointment_id] [int] IDENTITY(0,5) NOT NULL,
	[doctor_name] [varchar](50) NOT NULL,
	[patient_name] [varchar](50) NOT NULL,
	[appointment_location] [varchar](150) NOT NULL,
	[appointment_date] [date] NOT NULL,
	[appointment_time] [time](7) NOT NULL,
	[appointment_status] [nchar](10) NOT NULL,
 CONSTRAINT [PK_staff_appointments] PRIMARY KEY CLUSTERED 
(
	[appointment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_accounts]    Script Date: 4/21/2021 7:36:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_accounts](
	[account_id] [int] IDENTITY(1,1) NOT NULL,
	[account_username] [varchar](20) NOT NULL,
	[account_password] [varchar](20) NOT NULL,
	[account_confirmation] [varchar](20) NOT NULL,
	[account_status] [varchar](20) NOT NULL,
	[user_type] [varchar](20) NOT NULL,
 CONSTRAINT [PK_user_accounts] PRIMARY KEY CLUSTERED 
(
	[account_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [hospital_records] SET  READ_WRITE 
GO
